import { createApp } from 'vue'
import PrimeVue from 'primevue/config';
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import Card from 'primevue/card';
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';

//theme
import "primevue/resources/themes/lara-light-indigo/theme.css";	 
//core
import "primevue/resources/primevue.min.css";
//icons
import "primeicons/primeicons.css";

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(PrimeVue);
app.component('Card', Card);
app.component('InputText', InputText);
app.component('Button', Button);
app.mount('#app')